const level = [// timer and scoreboard
    "ZZZZZZZZZZZZZZZZA11111111111A",
    "A111122211111111110000000000A",//secret room with power up 
    "A000000000000122210000000000A",
    "A011111110100011111111111110A",
    "A000000010101000000000000000A",
    "A111111010101000033333330000A",
    "A000000010101000033333330000A",/// tunnel of blood .. eventually movement slowed.. maybe timer
    "A011111117101000033333330000A",
    "A000000177171000033333330000A",// 
    "A111100111171000000333000000A",
    "A889778888171000000000000000A",
    "A899988788111111111101111111A",
    "A889888988AA0000000000000000A",//dark forest on left.. impassible trees and drak cloud cover with pitfalls (left side of level)
    "A878899998AA0000000000000000A",
    "A878888978AA0000000000000000A",
    "A111440041AA0000000000000000A",
    "A111400444AA0000000000000000A",
    "A114404444AA0000000000000000A",
    "AAAA101AA1110111111111111111A",
    "AAAA101AA1100000000000000000A",
    "A111151111111111111111161111A"
]

const container = document.getElementById("container");


function createCell(newCell) {
    let cell = document.createElement("div");
    if (newCell === "0") cell.classList.add("floor");
    if (newCell === "1") cell.classList.add("wall");
    if (newCell === "2") cell.classList.add("skullWall");
    if (newCell === "3") cell.classList.add("bloodPond");
    if (newCell === "4") cell.classList.add("lava");
    if (newCell === "5") cell.classList.add("start");
    if (newCell === "6") cell.classList.add("finish");
    if (newCell === "7") cell.classList.add("spikes");
    if (newCell === "8") cell.classList.add("darkClouds");
    if (newCell === "9") cell.classList.add("tree");
    if (newCell === "A") cell.classList.add("test");
    if (newCell === "Z") cell.classList.add("nada");  
    return cell;
}

function createRow(id) {
    let row = document.createElement("div");
    row.id = id;
    row.classList.add("row");
    return row;
}

function drawMaze(level) {
    for (let y = 0; y < level.length; y++) {
        container.appendChild(createRow(y));
        for (let x = 0; x < level[0].length; x++) {
            let cell = createCell(level[y][x]);
            document.getElementById(y).appendChild(cell);
        }
    }
}

drawMaze(level);