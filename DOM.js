let p1 = document.createElement("div");
p1.className = "Player1";
game.appendChild(p1);

let p2 = document.createElement("div");
p2.className = "Player2";
game.appendChild(p2);

/// character select
let cs = document.createElement("div");
document.body.appendChild(cs);
cs.className = "charSelect";

let scoreBox = document.createElement("div");
scoreBox.setAttribute("id", "sBoard");
document.body.appendChild(scoreBox);
scoreBox.className = "sBoard";

let p2Hp = document.createElement("div");
p2Hp.setAttribute("id", "p2HP");
p2Hp.className = "hpLeft";
p2Hp.innerText = "P1 HP";
scoreBox.appendChild(p2Hp);

let p1Hp = document.createElement("div");
p1Hp.setAttribute("id", "p1HP");
p1Hp.className = "hpLeft";
p1Hp.innerText = "P2 HP";
scoreBox.appendChild(p1Hp);

/// player icons and select buttons
let fOne = document.createElement("div");
fOne.setAttribute("id", "Fighter1");
cs.appendChild(fOne);
document.getElementById("F1");
fOne.appendChild(F1);

let fTwo = document.createElement("div");
fTwo.setAttribute("id", "Fighter2");
cs.appendChild(fTwo);
document.getElementById("F2");
fTwo.appendChild(F2);